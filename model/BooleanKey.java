/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.annotation.JsonCreator
 *  com.fasterxml.jackson.annotation.JsonValue
 *  com.fasterxml.jackson.databind.annotation.JsonDeserialize
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.netflix.falcor.model.KeyType;
import com.netflix.falcor.model.SimpleKey;
import java.util.Objects;

@JsonDeserialize
public final class BooleanKey
extends SimpleKey {
    private final Boolean value;

    @JsonCreator
    public BooleanKey(Boolean value) {
        this.value = Objects.requireNonNull(value);
    }

    @JsonValue
    @Override
    public Boolean asBoolean() {
        return this.value;
    }

    @Override
    public KeyType getType() {
        return KeyType.BOOLEAN;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof BooleanKey) {
            BooleanKey that = (BooleanKey)o;
            return Objects.equals(this.value, that.value);
        }
        return false;
    }

    public int hashCode() {
        return this.value.hashCode();
    }
}

