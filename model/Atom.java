/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.annotation.JsonCreator
 *  com.fasterxml.jackson.annotation.JsonProperty
 *  com.fasterxml.jackson.annotation.JsonTypeName
 *  com.fasterxml.jackson.databind.JsonNode
 *  com.fasterxml.jackson.databind.ObjectMapper
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.falcor.model.FalcorValue;
import com.netflix.falcor.model.ValueType;
import java.util.Objects;

@JsonTypeName(value="atom")
public final class Atom
extends FalcorValue {
    private final JsonNode value;

    @JsonCreator
    public Atom(@JsonProperty(value="value") JsonNode value) {
        this.value = value;
    }

    public Atom(Object value) {
        this.value = mapper.valueToTree(value);
    }

    @JsonProperty(value="value")
    @Override
    public JsonNode asAtom() {
        return this.value;
    }

    @Override
    public ValueType getType() {
        return ValueType.ATOM;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof Atom) {
            Atom that = (Atom)o;
            return Objects.equals((Object)this.value, (Object)that.value);
        }
        return false;
    }

    public int hashCode() {
        return this.value.hashCode();
    }
}

