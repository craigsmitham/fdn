/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.core.JsonParser
 *  com.fasterxml.jackson.core.JsonProcessingException
 *  com.fasterxml.jackson.core.JsonToken
 *  com.fasterxml.jackson.databind.DeserializationContext
 *  com.fasterxml.jackson.databind.JsonDeserializer
 *  com.fasterxml.jackson.databind.JsonMappingException
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.netflix.falcor.model.BooleanKey;
import com.netflix.falcor.model.NullKey;
import com.netflix.falcor.model.SimpleKey;
import com.netflix.falcor.model.StringKey;
import java.io.IOException;

public class SimpleKeyDeserializer
extends JsonDeserializer<SimpleKey> {
    public SimpleKey deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        switch (p.getCurrentToken()) {
            case VALUE_TRUE: 
            case VALUE_FALSE: {
                return (SimpleKey)p.readValueAs((Class)BooleanKey.class);
            }
            case VALUE_STRING: {
                return (SimpleKey)p.readValueAs((Class)StringKey.class);
            }
        }
        throw ctxt.mappingException((Class)SimpleKey.class);
    }

    public SimpleKey getNullValue(DeserializationContext ctxt) {
        return NullKey.getInstance();
    }

}

