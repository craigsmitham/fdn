/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.annotation.JsonCreator
 *  com.fasterxml.jackson.annotation.JsonProperty
 *  com.fasterxml.jackson.databind.annotation.JsonDeserialize
 *  com.fasterxml.jackson.databind.annotation.JsonSerialize
 *  com.google.common.base.MoreObjects
 *  com.google.common.collect.ContiguousSet
 *  com.google.common.collect.DiscreteDomain
 *  com.google.common.collect.ImmutableRangeSet
 *  com.google.common.collect.Range
 *  com.google.common.collect.RangeSet
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.MoreObjects;
import com.google.common.collect.ContiguousSet;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.ImmutableRangeSet;
import com.google.common.collect.Range;
import com.google.common.collect.RangeSet;
import com.netflix.falcor.model.KeyType;
import com.netflix.falcor.model.NumberRangeSerializer;
import com.netflix.falcor.model.NumericKey;
import java.util.Objects;
import java.util.SortedSet;

@JsonDeserialize
@JsonSerialize(using=NumberRangeSerializer.class)
public final class NumberRange
extends NumericKey {
    private final Range<Long> value;

    @JsonCreator
    public NumberRange(@JsonProperty(value="from") Long from, @JsonProperty(value="to", required=1) Long to) {
        this(from, to, true);
    }

    public NumberRange(Long from, Long to, boolean inclusive) {
        Long lower = (Long)MoreObjects.firstNonNull((Object)from, (Object)0);
        this.value = inclusive ? Range.closed((Comparable)lower, (Comparable)to) : Range.closedOpen((Comparable)lower, (Comparable)to);
    }

    @Override
    public Range<Long> asRange() {
        return this.value;
    }

    @Override
    public RangeSet<Long> asRangeSet() {
        return ImmutableRangeSet.of(this.value);
    }

    @Override
    public SortedSet<Long> asNumericSet() {
        return ContiguousSet.create(this.value, (DiscreteDomain)DiscreteDomain.longs());
    }

    @Override
    public KeyType getType() {
        return KeyType.RANGE;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof NumberRange) {
            NumberRange that = (NumberRange)o;
            return Objects.equals(this.value, that.value);
        }
        return false;
    }

    public int hashCode() {
        return this.value.hashCode();
    }
}

