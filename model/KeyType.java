/*
 * Decompiled with CFR 0_102.
 */
package com.netflix.falcor.model;

public enum KeyType {
    STRING,
    BOOLEAN,
    NULL,
    NUMBER,
    RANGE,
    RANGE_SET,
    KEY_SET;
    

    private KeyType() {
    }
}

