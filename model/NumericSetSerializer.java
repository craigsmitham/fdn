/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.core.JsonGenerator
 *  com.fasterxml.jackson.databind.SerializerProvider
 *  com.fasterxml.jackson.databind.ser.std.StdSerializer
 *  com.google.common.collect.Range
 *  com.google.common.collect.RangeSet
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.google.common.collect.Range;
import com.google.common.collect.RangeSet;
import com.netflix.falcor.model.NumericSet;
import java.io.IOException;
import java.util.Set;

public class NumericSetSerializer
extends StdSerializer<NumericSet> {
    public NumericSetSerializer() {
        super((Class)NumericSet.class);
    }

    public void serialize(NumericSet value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartArray();
        for (Range range : value.asRangeSet().asRanges()) {
            if (((Long)range.lowerEndpoint()).equals(range.upperEndpoint())) {
                gen.writeNumber(((Long)range.lowerEndpoint()).longValue());
                continue;
            }
            gen.writeStartObject();
            if ((Long)range.lowerEndpoint() != 0) {
                gen.writeNumberField("from", ((Long)range.lowerEndpoint()).longValue());
            }
            gen.writeNumberField("to", ((Long)range.upperEndpoint()).longValue());
            gen.writeEndObject();
        }
        gen.writeEndArray();
    }
}

