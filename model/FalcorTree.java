/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.annotation.JsonTypeInfo
 *  com.fasterxml.jackson.annotation.JsonTypeInfo$Id
 *  com.fasterxml.jackson.databind.annotation.JsonDeserialize
 *  com.fasterxml.jackson.databind.annotation.JsonSerialize
 *  rx.functions.Func1
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.netflix.falcor.model.FalcorNode;
import com.netflix.falcor.model.FalcorTreeDeserializer;
import com.netflix.falcor.model.FalcorTreeSerializer;
import com.netflix.falcor.model.FalcorValue;
import com.netflix.falcor.model.KeySegment;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import rx.functions.Func1;

@JsonSerialize(using=FalcorTreeSerializer.class)
@JsonDeserialize(using=FalcorTreeDeserializer.class)
@JsonTypeInfo(use=JsonTypeInfo.Id.NONE)
public final class FalcorTree
extends FalcorNode {
    private final ConcurrentMap<KeySegment, FalcorNode> children = new ConcurrentHashMap<KeySegment, FalcorNode>();

    @Override
    public boolean isValue() {
        return false;
    }

    @Override
    public FalcorTree asTree() {
        return this;
    }

    @Override
    public <T> T match(Func1<FalcorValue, T> value, Func1<FalcorTree, T> tree) {
        return (T)tree.call((Object)this);
    }

    public ConcurrentMap<KeySegment, FalcorNode> children() {
        return this.children;
    }
}

