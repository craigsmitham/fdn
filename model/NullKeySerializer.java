/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.core.JsonGenerator
 *  com.fasterxml.jackson.databind.SerializerProvider
 *  com.fasterxml.jackson.databind.ser.std.StdSerializer
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.netflix.falcor.model.NullKey;
import java.io.IOException;

public class NullKeySerializer
extends StdSerializer<NullKey> {
    public NullKeySerializer() {
        super((Class)NullKey.class);
    }

    public void serialize(NullKey value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeNull();
    }
}

