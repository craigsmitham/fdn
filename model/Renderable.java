/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.core.JsonProcessingException
 *  com.fasterxml.jackson.databind.ObjectMapper
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public interface Renderable {
    public static final ObjectMapper mapper = new ObjectMapper();

    default public String asString() {
        try {
            return mapper.writeValueAsString((Object)this);
        }
        catch (JsonProcessingException e) {
            throw new RuntimeException((Throwable)e);
        }
    }

    default public byte[] asBytes() {
        try {
            return mapper.writeValueAsBytes((Object)this);
        }
        catch (JsonProcessingException e) {
            throw new RuntimeException((Throwable)e);
        }
    }
}

