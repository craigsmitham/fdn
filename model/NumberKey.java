/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.annotation.JsonCreator
 *  com.fasterxml.jackson.annotation.JsonValue
 *  com.fasterxml.jackson.databind.annotation.JsonDeserialize
 *  com.google.common.collect.ImmutableRangeSet
 *  com.google.common.collect.ImmutableSortedSet
 *  com.google.common.collect.Range
 *  com.google.common.collect.RangeSet
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.collect.ImmutableRangeSet;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Range;
import com.google.common.collect.RangeSet;
import com.netflix.falcor.model.KeyType;
import com.netflix.falcor.model.NumericKey;
import java.util.Objects;
import java.util.SortedSet;

@JsonDeserialize
public final class NumberKey
extends NumericKey {
    private final Long value;

    @JsonCreator
    public NumberKey(Long value) {
        this.value = value;
    }

    @JsonValue
    @Override
    public Long asLong() {
        return this.value;
    }

    @Override
    public Range<Long> asRange() {
        return Range.singleton((Comparable)this.value);
    }

    @Override
    public RangeSet<Long> asRangeSet() {
        return ImmutableRangeSet.of(this.asRange());
    }

    @Override
    public SortedSet<Long> asNumericSet() {
        return ImmutableSortedSet.of((Comparable)this.value);
    }

    @Override
    public KeyType getType() {
        return KeyType.NUMBER;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof NumberKey) {
            NumberKey that = (NumberKey)o;
            return Objects.equals(this.value, that.value);
        }
        return false;
    }

    public int hashCode() {
        return this.value.hashCode();
    }
}

