/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.core.JsonParser
 *  com.fasterxml.jackson.core.JsonProcessingException
 *  com.fasterxml.jackson.core.JsonToken
 *  com.fasterxml.jackson.databind.DeserializationContext
 *  com.fasterxml.jackson.databind.JsonDeserializer
 *  com.fasterxml.jackson.databind.JsonMappingException
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.netflix.falcor.model.NumberKey;
import com.netflix.falcor.model.NumberRange;
import com.netflix.falcor.model.NumericKey;
import java.io.IOException;

public class NumericKeyDeserializer
extends JsonDeserializer<NumericKey> {
    public NumericKey deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        switch (p.getCurrentToken()) {
            case VALUE_NUMBER_INT: {
                return (NumericKey)p.readValueAs((Class)NumberKey.class);
            }
            case START_OBJECT: {
                return (NumericKey)p.readValueAs((Class)NumberRange.class);
            }
        }
        throw ctxt.mappingException((Class)NumericKey.class);
    }

}

