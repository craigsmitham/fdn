/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.annotation.JsonAutoDetect
 *  com.fasterxml.jackson.annotation.JsonAutoDetect$Visibility
 *  com.fasterxml.jackson.annotation.JsonInclude
 *  com.fasterxml.jackson.annotation.JsonInclude$Include
 *  com.fasterxml.jackson.annotation.JsonSubTypes
 *  com.fasterxml.jackson.annotation.JsonSubTypes$Type
 *  com.fasterxml.jackson.annotation.JsonTypeInfo
 *  com.fasterxml.jackson.annotation.JsonTypeInfo$As
 *  com.fasterxml.jackson.annotation.JsonTypeInfo$Id
 *  com.fasterxml.jackson.databind.JsonNode
 *  rx.functions.Func1
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.JsonNode;
import com.netflix.falcor.model.Atom;
import com.netflix.falcor.model.Error;
import com.netflix.falcor.model.FalcorNode;
import com.netflix.falcor.model.FalcorPath;
import com.netflix.falcor.model.FalcorTree;
import com.netflix.falcor.model.Ref;
import com.netflix.falcor.model.ValueType;
import rx.functions.Func1;

@JsonInclude(content=JsonInclude.Include.NON_NULL)
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY, property="$type")
@JsonSubTypes(value={@JsonSubTypes.Type(value=Atom.class), @JsonSubTypes.Type(value=Ref.class), @JsonSubTypes.Type(value=Error.class)})
@JsonAutoDetect(getterVisibility=JsonAutoDetect.Visibility.NONE, isGetterVisibility=JsonAutoDetect.Visibility.NONE)
public abstract class FalcorValue
extends FalcorNode {
    @Override
    public boolean isValue() {
        return false;
    }

    @Override
    public FalcorValue asValue() {
        return this;
    }

    @Override
    public <T> T match(Func1<FalcorValue, T> value, Func1<FalcorTree, T> tree) {
        return (T)value.call((Object)this);
    }

    public abstract ValueType getType();

    public final boolean isAtom() {
        return this.getType() == ValueType.ATOM;
    }

    public final boolean isRef() {
        return this.getType() == ValueType.REF;
    }

    public final boolean isError() {
        return this.getType() == ValueType.ERROR;
    }

    public JsonNode asAtom() {
        return null;
    }

    public FalcorPath asRef() {
        return null;
    }

    public String asError() {
        return null;
    }
}

