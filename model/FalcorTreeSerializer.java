/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.core.JsonGenerator
 *  com.fasterxml.jackson.databind.SerializerProvider
 *  com.fasterxml.jackson.databind.ser.std.StdSerializer
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.netflix.falcor.model.FalcorNode;
import com.netflix.falcor.model.FalcorTree;
import com.netflix.falcor.model.KeySegment;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

public class FalcorTreeSerializer
extends StdSerializer<FalcorTree> {
    public FalcorTreeSerializer() {
        super((Class)FalcorTree.class);
    }

    public void serialize(FalcorTree tree, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeStartObject();
        for (Map.Entry<KeySegment, FalcorNode> entry : tree.children().entrySet()) {
            gen.writeObjectField(entry.getKey().toString(), (Object)entry.getValue());
        }
        gen.writeEndObject();
    }
}

