/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.core.JsonParser
 *  com.fasterxml.jackson.core.JsonProcessingException
 *  com.fasterxml.jackson.core.JsonToken
 *  com.fasterxml.jackson.databind.DeserializationContext
 *  com.fasterxml.jackson.databind.JsonDeserializer
 *  com.fasterxml.jackson.databind.JsonMappingException
 *  com.fasterxml.jackson.databind.util.TokenBuffer
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.util.TokenBuffer;
import com.netflix.falcor.model.KeySegment;
import com.netflix.falcor.model.KeySet;
import com.netflix.falcor.model.NullKey;
import com.netflix.falcor.model.NumericKey;
import com.netflix.falcor.model.NumericSet;
import com.netflix.falcor.model.SimpleKey;
import java.io.IOException;

public class KeySegmentDeserializer
extends JsonDeserializer<KeySegment> {
    public KeySegment deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        switch (p.getCurrentToken()) {
            case VALUE_TRUE: 
            case VALUE_FALSE: 
            case VALUE_STRING: {
                return (KeySegment)p.readValueAs((Class)SimpleKey.class);
            }
            case VALUE_NUMBER_INT: 
            case START_OBJECT: {
                return (KeySegment)p.readValueAs((Class)NumericKey.class);
            }
            case START_ARRAY: {
                TokenBuffer buf = new TokenBuffer(p);
                buf.copyCurrentStructure(p);
                try {
                    return (KeySegment)buf.asParser().readValueAs((Class)KeySet.class);
                }
                catch (IOException e) {
                    return (KeySegment)buf.asParser().readValueAs((Class)NumericSet.class);
                }
            }
        }
        throw ctxt.mappingException((Class)KeySegment.class);
    }

    public SimpleKey getNullValue(DeserializationContext ctxt) {
        return NullKey.getInstance();
    }

}

