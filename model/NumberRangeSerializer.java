/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.core.JsonGenerator
 *  com.fasterxml.jackson.databind.SerializerProvider
 *  com.fasterxml.jackson.databind.ser.std.StdSerializer
 *  com.google.common.collect.Range
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.google.common.collect.Range;
import com.netflix.falcor.model.NumberRange;
import java.io.IOException;

public class NumberRangeSerializer
extends StdSerializer<NumberRange> {
    public NumberRangeSerializer() {
        super((Class)NumberRange.class);
    }

    public void serialize(NumberRange value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        Range<Long> range = value.asRange();
        gen.writeStartObject();
        if ((Long)range.lowerEndpoint() != 0) {
            gen.writeNumberField("from", ((Long)range.lowerEndpoint()).longValue());
        }
        gen.writeNumberField("to", ((Long)range.upperEndpoint()).longValue());
        gen.writeEndObject();
    }
}

