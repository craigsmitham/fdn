/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.annotation.JsonCreator
 *  com.fasterxml.jackson.annotation.JsonValue
 *  com.fasterxml.jackson.databind.annotation.JsonDeserialize
 *  com.google.common.collect.ImmutableSet
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.collect.ImmutableSet;
import com.netflix.falcor.model.KeySegment;
import com.netflix.falcor.model.KeyType;
import com.netflix.falcor.model.SimpleKey;
import java.util.Objects;
import java.util.Set;

@JsonDeserialize
public final class KeySet
extends KeySegment {
    private final ImmutableSet<SimpleKey> value;

    @JsonCreator
    public /* varargs */ KeySet(SimpleKey ... keys) {
        this.value = ImmutableSet.copyOf((Object[])keys);
    }

    public KeySet(Iterable<? extends SimpleKey> keys) {
        this.value = ImmutableSet.copyOf(keys);
    }

    @JsonValue
    @Override
    public Set<SimpleKey> asKeySet() {
        return this.value;
    }

    @Override
    public KeyType getType() {
        return KeyType.KEY_SET;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof KeySet) {
            KeySet that = (KeySet)o;
            return Objects.equals(this.value, that.value);
        }
        return false;
    }

    public int hashCode() {
        return this.value.hashCode();
    }
}

