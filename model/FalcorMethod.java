/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.annotation.JsonProperty
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.netflix.falcor.model.Renderable;

public enum FalcorMethod implements Renderable
{
    GET,
    SET,
    CALL;
    

    private FalcorMethod() {
    }

    public String toString() {
        return this.asString();
    }
}

