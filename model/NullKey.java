/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.databind.annotation.JsonSerialize
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.netflix.falcor.model.KeyType;
import com.netflix.falcor.model.NullKeySerializer;
import com.netflix.falcor.model.SimpleKey;

@JsonSerialize(using=NullKeySerializer.class)
public final class NullKey
extends SimpleKey {
    public static final NullKey instance = new NullKey();

    private NullKey() {
    }

    public static NullKey getInstance() {
        return instance;
    }

    @Override
    public KeyType getType() {
        return KeyType.NULL;
    }

    public boolean equals(Object o) {
        return o == this;
    }
}

