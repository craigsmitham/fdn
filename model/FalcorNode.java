/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.annotation.JsonSubTypes
 *  com.fasterxml.jackson.annotation.JsonSubTypes$Type
 *  com.fasterxml.jackson.annotation.JsonTypeInfo
 *  com.fasterxml.jackson.annotation.JsonTypeInfo$As
 *  com.fasterxml.jackson.annotation.JsonTypeInfo$Id
 *  com.google.common.collect.Maps
 *  rx.functions.Func1
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.google.common.collect.Maps;
import com.netflix.falcor.model.FalcorPath;
import com.netflix.falcor.model.FalcorTree;
import com.netflix.falcor.model.FalcorValue;
import com.netflix.falcor.model.KeySegment;
import com.netflix.falcor.model.PathValue;
import com.netflix.falcor.model.Renderable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;
import java.util.function.ToIntFunction;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import rx.functions.Func1;

@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY, property="$type", defaultImpl=FalcorTree.class)
@JsonSubTypes(value={@JsonSubTypes.Type(value=FalcorValue.class)})
public abstract class FalcorNode
implements Renderable {
    public abstract boolean isValue();

    public boolean isTree() {
        return !this.isValue();
    }

    public FalcorValue asValue() {
        return null;
    }

    public FalcorTree asTree() {
        return null;
    }

    public abstract <T> T match(Func1<FalcorValue, T> var1, Func1<FalcorTree, T> var2);

    public <T> T fold(Func1<FalcorValue, T> f1, Func1<Map<KeySegment, T>, T> f2) {
        return this.match(f1, tree -> f2.call((Object)Maps.transformValues(tree.children(), v -> v.fold(func12, func1))));
    }

    public int size() {
        return (Integer)this.match(value -> 1, tree -> tree.children().values().stream().mapToInt(FalcorNode::size).sum());
    }

    public FalcorValue get(FalcorPath path) {
        return (FalcorValue)this.match(value -> value, tree -> {
            FalcorNode child;
            if (!(path.isEmpty() || (child = tree.children().get(path.head())) == null)) {
                return child.get(path.tail());
            }
            return null;
        }
        );
    }

    public boolean contains(FalcorPath path) {
        return this.get(path) != null;
    }

    public Stream<PathValue> traverse(FalcorPath path) {
        return (Stream)this.match(value -> Stream.of(new PathValue(path, value)), tree -> tree.children().entrySet().stream().flatMap(entry -> ((FalcorNode)entry.getValue()).traverse(falcorPath.append((KeySegment)entry.getKey()))));
    }

    public FalcorNode put(FalcorPath path, FalcorValue newValue) {
        if (path.isEmpty()) {
            throw new IllegalArgumentException("path is empty");
        }
        return (FalcorNode)this.match(value -> {
            throw new UnsupportedOperationException("put on value node");
        }
        , tree -> {
            KeySegment head = path.head();
            FalcorPath tail = path.tail();
            if (tail.isEmpty()) {
                return tree.children().put(head, newValue);
            }
            return tree.children().computeIfAbsent(head, key -> new FalcorTree()).put(tail, newValue);
        }
        );
    }

    public void putAll(FalcorTree other) {
        this.match(value -> {
            throw new UnsupportedOperationException("putAll on value node");
        }
        , tree -> {
            for (Map.Entry<KeySegment, FalcorNode> entry : other.children().entrySet()) {
                FalcorNode child = entry.getValue();
                if (child.isValue()) {
                    tree.children().put(entry.getKey(), child);
                    continue;
                }
                tree.children().computeIfAbsent(entry.getKey(), key -> new FalcorTree()).putAll(child.asTree());
            }
            return null;
        }
        );
    }

    public String toString() {
        return this.asString();
    }
}

