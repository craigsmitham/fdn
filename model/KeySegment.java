/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.annotation.JsonAutoDetect
 *  com.fasterxml.jackson.annotation.JsonAutoDetect$Visibility
 *  com.fasterxml.jackson.databind.annotation.JsonDeserialize
 *  com.google.common.collect.ImmutableRangeSet
 *  com.google.common.collect.Range
 *  com.google.common.collect.RangeSet
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.collect.ImmutableRangeSet;
import com.google.common.collect.Range;
import com.google.common.collect.RangeSet;
import com.netflix.falcor.model.KeySegmentDeserializer;
import com.netflix.falcor.model.KeyType;
import com.netflix.falcor.model.Renderable;
import com.netflix.falcor.model.SimpleKey;
import java.util.Collections;
import java.util.Set;
import java.util.SortedSet;

@JsonDeserialize(using=KeySegmentDeserializer.class)
@JsonAutoDetect(getterVisibility=JsonAutoDetect.Visibility.NONE, isGetterVisibility=JsonAutoDetect.Visibility.NONE)
public abstract class KeySegment
implements Renderable {
    public abstract KeyType getType();

    public final boolean isString() {
        return this.getType() == KeyType.STRING;
    }

    public final boolean isBoolean() {
        return this.getType() == KeyType.BOOLEAN;
    }

    public final boolean isNull() {
        return this.getType() == KeyType.NULL;
    }

    public final boolean isNumber() {
        return this.getType() == KeyType.NUMBER;
    }

    public final boolean isRange() {
        return this.getType() == KeyType.RANGE;
    }

    public final boolean isRangeSet() {
        return this.getType() == KeyType.RANGE_SET;
    }

    public final boolean isNumeric() {
        return this.isNumber() || this.isRange();
    }

    public final boolean isNumericSet() {
        return this.isNumeric() || this.isRangeSet();
    }

    public final boolean isKeySet() {
        return this.getType() == KeyType.KEY_SET;
    }

    public Boolean asBoolean() {
        return false;
    }

    public Long asLong() {
        return 0;
    }

    public Range<Long> asRange() {
        return Range.openClosed((Comparable)Long.valueOf(0), (Comparable)Long.valueOf(0));
    }

    public RangeSet<Long> asRangeSet() {
        return ImmutableRangeSet.of();
    }

    public SortedSet<Long> asNumericSet() {
        return Collections.emptySortedSet();
    }

    public Set<SimpleKey> asKeySet() {
        return Collections.emptySet();
    }

    public String toString() {
        return this.asString();
    }
}

