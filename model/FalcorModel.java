/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.annotation.JsonProperty
 *  com.fasterxml.jackson.databind.JsonNode
 *  com.fasterxml.jackson.databind.ObjectMapper
 *  com.fasterxml.jackson.databind.node.MissingNode
 *  com.fasterxml.jackson.databind.node.NullNode
 *  com.fasterxml.jackson.databind.node.ObjectNode
 *  rx.functions.Func1
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.MissingNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.netflix.falcor.model.FalcorNode;
import com.netflix.falcor.model.FalcorPath;
import com.netflix.falcor.model.FalcorTree;
import com.netflix.falcor.model.FalcorValue;
import com.netflix.falcor.model.KeySegment;
import com.netflix.falcor.model.PathValue;
import com.netflix.falcor.model.Renderable;
import com.netflix.falcor.model.ValueType;
import java.util.Iterator;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Stream;
import rx.functions.Func1;

public final class FalcorModel
implements Renderable,
Iterable<PathValue> {
    private final FalcorTree cache;

    public FalcorModel() {
        this(new FalcorTree());
    }

    public FalcorModel(FalcorTree cache) {
        this.cache = cache;
    }

    @JsonProperty(value="jsong")
    public FalcorTree getCache() {
        return this.cache;
    }

    public JsonNode toJson() {
        return mapper.createObjectNode().set("json", (JsonNode)this.cache.fold(value -> {
            while (value != null) {
                switch (value.getType()) {
                    case ATOM: {
                        return value.asAtom();
                    }
                    case ERROR: {
                        return NullNode.getInstance();
                    }
                    case REF: {
                        value = this.cache.get(value.asRef());
                    }
                }
            }
            return MissingNode.getInstance();
        }
        , map -> {
            ObjectNode node = mapper.createObjectNode();
            map.forEach((k, v) -> {
                node.set(k.toString(), v);
            }
            );
            return node;
        }
        ));
    }

    public int size() {
        return this.cache.size();
    }

    public FalcorValue get(FalcorPath path) {
        return this.cache.get(path);
    }

    public boolean contains(FalcorPath path) {
        return this.cache.contains(path);
    }

    public FalcorNode put(FalcorPath path, FalcorValue value) {
        return this.cache.put(path, value);
    }

    public void merge(FalcorTree other) {
        this.cache.putAll(other);
    }

    @Override
    public Iterator<PathValue> iterator() {
        return this.cache.traverse(FalcorPath.empty()).iterator();
    }

    public String toString() {
        return this.asString();
    }

}

