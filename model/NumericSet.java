/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.annotation.JsonCreator
 *  com.fasterxml.jackson.databind.annotation.JsonDeserialize
 *  com.fasterxml.jackson.databind.annotation.JsonSerialize
 *  com.google.common.base.Suppliers
 *  com.google.common.collect.DiscreteDomain
 *  com.google.common.collect.ImmutableRangeSet
 *  com.google.common.collect.ImmutableSortedSet
 *  com.google.common.collect.Range
 *  com.google.common.collect.RangeSet
 *  com.google.common.collect.TreeRangeSet
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Suppliers;
import com.google.common.collect.DiscreteDomain;
import com.google.common.collect.ImmutableRangeSet;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Range;
import com.google.common.collect.RangeSet;
import com.google.common.collect.TreeRangeSet;
import com.netflix.falcor.model.KeySegment;
import com.netflix.falcor.model.KeyType;
import com.netflix.falcor.model.NumericKey;
import com.netflix.falcor.model.NumericSetSerializer;
import java.util.Arrays;
import java.util.Objects;
import java.util.SortedSet;

@JsonDeserialize
@JsonSerialize(using=NumericSetSerializer.class)
public final class NumericSet
extends KeySegment {
    final ImmutableRangeSet<Long> value;

    @JsonCreator
    public /* varargs */ NumericSet(NumericKey ... keys) {
        this(Arrays.asList(keys));
    }

    public NumericSet(Iterable<? extends NumericKey> keys) {
        TreeRangeSet rangeSet = TreeRangeSet.create();
        for (NumericKey key : keys) {
            rangeSet.add(key.asRange());
        }
        this.value = ImmutableRangeSet.copyOf((RangeSet)rangeSet);
    }

    @Override
    public RangeSet<Long> asRangeSet() {
        return this.value;
    }

    @Override
    public SortedSet<Long> asNumericSet() {
        return (SortedSet)Suppliers.memoize(() -> this.value.asSet(DiscreteDomain.longs())).get();
    }

    @Override
    public KeyType getType() {
        return KeyType.RANGE_SET;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof NumericSet) {
            NumericSet that = (NumericSet)o;
            return Objects.equals(this.value, that.value);
        }
        return false;
    }

    public int hashCode() {
        return this.value.hashCode();
    }
}

