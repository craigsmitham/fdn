/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.databind.annotation.JsonDeserialize
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.netflix.falcor.model.KeySegment;
import com.netflix.falcor.model.SimpleKeyDeserializer;
import java.util.Collections;
import java.util.Set;

@JsonDeserialize(using=SimpleKeyDeserializer.class)
public abstract class SimpleKey
extends KeySegment {
    @Override
    public Set<SimpleKey> asKeySet() {
        return Collections.singleton(this);
    }
}

