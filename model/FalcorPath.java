/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.annotation.JsonCreator
 *  com.fasterxml.jackson.databind.annotation.JsonSerialize
 *  com.google.common.collect.Iterables
 *  javaslang.collection.List
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Iterables;
import com.netflix.falcor.model.KeySegment;
import com.netflix.falcor.model.Renderable;
import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import javaslang.collection.List;

@JsonSerialize(as=Iterable.class)
public final class FalcorPath
implements Renderable,
Iterable<KeySegment> {
    private static final FalcorPath EMPTY = new FalcorPath(List.nil(), List.nil());
    private final List<KeySegment> front;
    private final List<KeySegment> rear;

    private FalcorPath(List<KeySegment> front, List<KeySegment> rear) {
        boolean isFrontEmpty = front.isEmpty();
        this.front = isFrontEmpty ? rear.reverse() : front;
        this.rear = isFrontEmpty ? front : rear;
    }

    public static FalcorPath empty() {
        return EMPTY;
    }

    public static FalcorPath of(KeySegment key) {
        return new FalcorPath(List.of((Object)key), List.nil());
    }

    @JsonCreator
    public static /* varargs */ FalcorPath of(KeySegment ... keys) {
        return new FalcorPath(List.of((Object[])keys), List.nil());
    }

    public static FalcorPath ofAll(Iterable<? extends KeySegment> keys) {
        return keys instanceof FalcorPath ? (FalcorPath)keys : new FalcorPath(List.ofAll(keys), List.nil());
    }

    public FalcorPath append(KeySegment key) {
        return new FalcorPath(this.front, this.rear.prepend((Object)key));
    }

    public /* varargs */ FalcorPath append(KeySegment ... keys) {
        return this.appendAll(Arrays.asList(keys));
    }

    public FalcorPath appendAll(Iterable<? extends KeySegment> keys) {
        List temp = this.rear;
        for (KeySegment key : keys) {
            temp = temp.prepend((Object)key);
        }
        return new FalcorPath(this.front, temp);
    }

    public FalcorPath prepend(KeySegment key) {
        return new FalcorPath(this.front.prepend((Object)key), this.rear);
    }

    public FalcorPath prependAll(Iterable<? extends KeySegment> keys) {
        return new FalcorPath(this.front.prependAll(keys), this.rear);
    }

    public boolean isEmpty() {
        return this.front.isEmpty();
    }

    @Override
    public Iterator<KeySegment> iterator() {
        return new Iterator<KeySegment>(){
            FalcorPath path;

            @Override
            public boolean hasNext() {
                return !this.path.isEmpty();
            }

            @Override
            public KeySegment next() {
                if (this.path.isEmpty()) {
                    throw new NoSuchElementException();
                }
                KeySegment result = this.path.head();
                this.path = this.path.tail();
                return result;
            }
        };
    }

    public FalcorPath drop(int n) {
        return new FalcorPath(this.front.drop(n), this.rear.dropRight(n - this.front.length()));
    }

    public FalcorPath dropRight(int n) {
        return new FalcorPath(this.front.dropRight(n), this.rear.drop(n - this.front.length()));
    }

    public KeySegment head() {
        if (this.isEmpty()) {
            throw new NoSuchElementException("head of empty path");
        }
        return (KeySegment)this.front.head();
    }

    public Optional<? extends KeySegment> headOption() {
        return this.isEmpty() ? Optional.empty() : Optional.of(this.front.head());
    }

    public FalcorPath init() {
        if (this.isEmpty()) {
            throw new UnsupportedOperationException("init of empty path");
        }
        if (this.rear.isEmpty()) {
            return new FalcorPath(this.front.init(), this.rear);
        }
        return new FalcorPath(this.front, this.rear.tail());
    }

    public Optional<? extends FalcorPath> initOption() {
        return this.isEmpty() ? Optional.empty() : Optional.of(this.init());
    }

    public FalcorPath tail() {
        if (this.isEmpty()) {
            throw new UnsupportedOperationException("tail of empty path");
        }
        return new FalcorPath(this.front.tail(), this.rear);
    }

    public Optional<? extends FalcorPath> tailOption() {
        return this.isEmpty() ? Optional.empty() : Optional.of(this.tail());
    }

    public FalcorPath take(int n) {
        int frontLength = this.front.length();
        if (n < frontLength) {
            return new FalcorPath(this.front.take(n), List.nil());
        }
        if (n == frontLength) {
            return new FalcorPath(this.front, List.nil());
        }
        return new FalcorPath(this.front, this.rear.takeRight(n - frontLength));
    }

    public FalcorPath takeRight(int n) {
        int rearLength = this.rear.length();
        if (n < rearLength) {
            return new FalcorPath(this.rear.take(n).reverse(), List.nil());
        }
        if (n == rearLength) {
            return new FalcorPath(this.rear.reverse(), List.nil());
        }
        return new FalcorPath(this.front.takeRight(n - rearLength), this.rear);
    }

    public String toString() {
        return this.asString();
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof FalcorPath) {
            FalcorPath that = (FalcorPath)o;
            return Iterables.elementsEqual((Iterable)this, (Iterable)that);
        }
        return false;
    }

    public int hashCode() {
        int hashCode = 1;
        for (KeySegment key : this) {
            hashCode = 31 * hashCode + Objects.hashCode(key);
        }
        return hashCode;
    }

}

