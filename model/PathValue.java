/*
 * Decompiled with CFR 0_102.
 */
package com.netflix.falcor.model;

import com.netflix.falcor.model.FalcorPath;
import com.netflix.falcor.model.FalcorValue;
import java.util.AbstractMap;

public class PathValue
extends AbstractMap.SimpleImmutableEntry<FalcorPath, FalcorValue> {
    public PathValue(FalcorPath path, FalcorValue value) {
        super(path, value);
    }
}

