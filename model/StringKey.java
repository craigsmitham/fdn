/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.annotation.JsonCreator
 *  com.fasterxml.jackson.annotation.JsonValue
 *  com.fasterxml.jackson.databind.annotation.JsonDeserialize
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.netflix.falcor.model.KeyType;
import com.netflix.falcor.model.SimpleKey;
import java.util.Objects;

@JsonDeserialize
public final class StringKey
extends SimpleKey {
    private final String value;

    @JsonCreator
    public StringKey(String value) {
        this.value = Objects.requireNonNull(value);
    }

    @JsonValue
    @Override
    public String asString() {
        return this.value;
    }

    @Override
    public KeyType getType() {
        return KeyType.STRING;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof StringKey) {
            StringKey that = (StringKey)o;
            return Objects.equals(this.value, that.value);
        }
        return false;
    }

    public int hashCode() {
        return this.value.hashCode();
    }
}

