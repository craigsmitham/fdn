/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.databind.annotation.JsonDeserialize
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.netflix.falcor.model.KeySegment;
import com.netflix.falcor.model.NumericKeyDeserializer;

@JsonDeserialize(using=NumericKeyDeserializer.class)
public abstract class NumericKey
extends KeySegment {
}

