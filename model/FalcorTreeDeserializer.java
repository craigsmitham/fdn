/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.core.JsonFactory
 *  com.fasterxml.jackson.core.JsonParser
 *  com.fasterxml.jackson.core.JsonProcessingException
 *  com.fasterxml.jackson.core.JsonToken
 *  com.fasterxml.jackson.core.ObjectCodec
 *  com.fasterxml.jackson.databind.DeserializationContext
 *  com.fasterxml.jackson.databind.JsonDeserializer
 *  com.fasterxml.jackson.databind.JsonMappingException
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.netflix.falcor.model.FalcorNode;
import com.netflix.falcor.model.FalcorTree;
import com.netflix.falcor.model.KeySegment;
import com.netflix.falcor.model.StringKey;
import java.io.IOException;
import java.util.concurrent.ConcurrentMap;

public class FalcorTreeDeserializer
extends JsonDeserializer<FalcorTree> {
    public FalcorTree deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonToken t = jp.getCurrentToken();
        if (t == JsonToken.START_OBJECT) {
            t = jp.nextToken();
        }
        if (t != JsonToken.FIELD_NAME && t != JsonToken.END_OBJECT) {
            throw ctxt.mappingException((Class)FalcorTree.class);
        }
        return FalcorTreeDeserializer._deserializeEntries(jp, ctxt);
    }

    private static FalcorTree _deserializeEntries(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonFactory factory = new JsonFactory(jp.getCodec());
        FalcorTree node = new FalcorTree();
        while (jp.getCurrentToken() == JsonToken.FIELD_NAME) {
            KeySegment key;
            String fieldName = jp.getCurrentName();
            try {
                key = (KeySegment)factory.createParser(fieldName).readValueAs((Class)KeySegment.class);
            }
            catch (IOException e) {
                key = new StringKey(fieldName);
            }
            JsonToken t = jp.nextToken();
            FalcorNode value = (FalcorNode)jp.readValueAs((Class)FalcorNode.class);
            node.children().put(key, value);
            jp.nextToken();
        }
        return node;
    }
}

