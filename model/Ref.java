/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.annotation.JsonCreator
 *  com.fasterxml.jackson.annotation.JsonProperty
 *  com.fasterxml.jackson.annotation.JsonTypeName
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.netflix.falcor.model.FalcorPath;
import com.netflix.falcor.model.FalcorValue;
import com.netflix.falcor.model.KeySegment;
import com.netflix.falcor.model.ValueType;
import java.util.Iterator;
import java.util.Objects;

@JsonTypeName(value="ref")
public final class Ref
extends FalcorValue
implements Iterable<KeySegment> {
    private final FalcorPath path;

    @JsonCreator
    public Ref(@JsonProperty(value="value") FalcorPath path) {
        this.path = path;
    }

    @JsonProperty(value="value")
    @Override
    public FalcorPath asRef() {
        return this.path;
    }

    @Override
    public Iterator<KeySegment> iterator() {
        return this.path.iterator();
    }

    @Override
    public ValueType getType() {
        return ValueType.REF;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof Ref) {
            Ref that = (Ref)o;
            return Objects.equals(this.path, that.path);
        }
        return false;
    }

    public int hashCode() {
        return this.path.hashCode();
    }
}

