/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.fasterxml.jackson.annotation.JsonCreator
 *  com.fasterxml.jackson.annotation.JsonProperty
 *  com.fasterxml.jackson.annotation.JsonTypeName
 */
package com.netflix.falcor.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.netflix.falcor.model.FalcorValue;
import com.netflix.falcor.model.ValueType;
import java.util.Objects;

@JsonTypeName(value="error")
public final class Error
extends FalcorValue {
    private final String error;

    @JsonCreator
    public Error(@JsonProperty(value="value") String error) {
        this.error = error;
    }

    @JsonProperty(value="value")
    @Override
    public String asError() {
        return this.error;
    }

    @Override
    public ValueType getType() {
        return ValueType.ERROR;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof Error) {
            Error that = (Error)o;
            return Objects.equals(this.error, that.error);
        }
        return false;
    }

    public int hashCode() {
        return this.error.hashCode();
    }
}

