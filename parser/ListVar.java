/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.github.fge.grappa.support.Var
 *  com.google.common.base.Supplier
 *  com.google.common.collect.Iterables
 */
package com.netflix.falcor.parser;

import com.github.fge.grappa.support.Var;
import com.google.common.base.Supplier;
import com.google.common.collect.Iterables;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class ListVar<E>
extends Var<List<E>>
implements Iterable<E> {
    public ListVar() {
        super(ArrayList::new);
    }

    public int size() {
        return ((List)this.getNonnull()).size();
    }

    public boolean isEmpty() {
        return ((List)this.getNonnull()).isEmpty();
    }

    public boolean contains(Object object) {
        return ((List)this.getNonnull()).contains(object);
    }

    @Override
    public Iterator<E> iterator() {
        return ((List)this.getNonnull()).iterator();
    }

    public boolean add(E element) {
        return ((List)this.getNonnull()).add(element);
    }

    public boolean remove(Object object) {
        return ((List)this.getNonnull()).remove(object);
    }

    public boolean containsAll(Collection<?> collection) {
        return ((List)this.getNonnull()).containsAll(collection);
    }

    public boolean addAll(Iterable<? extends E> elements) {
        return Iterables.addAll((Collection)((Collection)this.getNonnull()), elements);
    }

    public boolean retainAll(Collection<?> collection) {
        return ((List)this.getNonnull()).retainAll(collection);
    }
}

