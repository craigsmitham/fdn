/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  com.github.fge.grappa.matchers.join.JoinMatcherBootstrap
 *  com.github.fge.grappa.matchers.join.JoinMatcherBuilder
 *  com.github.fge.grappa.parsers.BaseParser
 *  com.github.fge.grappa.rules.Rule
 *  com.github.fge.grappa.run.ListeningParseRunner
 *  com.github.fge.grappa.run.ParsingResult
 *  com.github.fge.grappa.support.StringVar
 *  com.github.fge.grappa.support.Var
 */
package com.netflix.falcor.parser;

import com.github.fge.grappa.matchers.join.JoinMatcherBootstrap;
import com.github.fge.grappa.matchers.join.JoinMatcherBuilder;
import com.github.fge.grappa.parsers.BaseParser;
import com.github.fge.grappa.rules.Rule;
import com.github.fge.grappa.run.ListeningParseRunner;
import com.github.fge.grappa.run.ParsingResult;
import com.github.fge.grappa.support.StringVar;
import com.github.fge.grappa.support.Var;
import com.netflix.falcor.model.BooleanKey;
import com.netflix.falcor.model.FalcorPath;
import com.netflix.falcor.model.KeySegment;
import com.netflix.falcor.model.KeySet;
import com.netflix.falcor.model.NullKey;
import com.netflix.falcor.model.NumberKey;
import com.netflix.falcor.model.NumberRange;
import com.netflix.falcor.model.NumericKey;
import com.netflix.falcor.model.NumericSet;
import com.netflix.falcor.model.SimpleKey;
import com.netflix.falcor.model.StringKey;
import com.netflix.falcor.parser.ListVar;

public class FalcorParser
extends BaseParser<FalcorPath> {
    Rule whiteSpaces() {
        return this.join((Object)this.zeroOrMore((Object)this.wsp())).using((Object)this.sequence((Object)this.optional((Object)this.cr()), (Object)this.lf(), new Object[0])).min(0);
    }

    Rule charUnescaped() {
        return this.sequence((Object)this.testNot((Object)this.firstOf((Object)this.anyOf("\\\""), (Object)this.ctl(), new Object[0])), (Object)this.unicodeRange(0, 1114111), new Object[0]);
    }

    Rule charEscaped() {
        return this.sequence((Object)Character.valueOf('\\'), (Object)this.firstOf((Object)this.anyOf("\"\\/bfnrt"), (Object)this.sequence((Object)Character.valueOf('u'), (Object)this.nTimes(4, (Object)this.hexDigit()), new Object[0]), new Object[0]), new Object[0]);
    }

    Rule character() {
        return this.firstOf((Object)this.charUnescaped(), (Object)this.charEscaped(), new Object[0]);
    }

    Rule identifier(ListVar<? super SimpleKey> keys) {
        return this.sequence((Object)this.oneOrMore((Object)this.firstOf((Object)this.alpha(), (Object)this.digit(), new Object[]{this.anyOf("_$")})), (Object)keys.add((SimpleKey)new StringKey(this.match())), new Object[0]);
    }

    Rule booleanKey(ListVar<? super SimpleKey> keys) {
        return this.firstOf((Object)this.sequence((Object)"true", (Object)keys.add((SimpleKey)new BooleanKey(true)), new Object[0]), (Object)this.sequence((Object)"false", (Object)keys.add((SimpleKey)new BooleanKey(false)), new Object[0]), new Object[0]);
    }

    Rule nullKey(ListVar<? super SimpleKey> keys) {
        return this.sequence((Object)"null", (Object)keys.add((SimpleKey)NullKey.getInstance()), new Object[0]);
    }

    Rule stringKey(ListVar<? super SimpleKey> keys) {
        StringVar string = new StringVar();
        return this.sequence((Object)Character.valueOf('\"'), (Object)this.zeroOrMore((Object)this.character()), new Object[]{string.set((Object)this.match()), Character.valueOf('\"'), keys.add((SimpleKey)new StringKey((String)string.get()))});
    }

    Rule key(ListVar<? super SimpleKey> keys) {
        return this.firstOf((Object)this.booleanKey(keys), (Object)this.nullKey(keys), new Object[]{this.stringKey(keys)});
    }

    Rule keySet(ListVar<KeySegment> path) {
        ListVar keys = new ListVar();
        Object[] arrobject = new Object[4];
        arrobject[0] = this.join((Object)this.key(keys)).using((Object)this.sequence((Object)this.whiteSpaces(), (Object)Character.valueOf(','), new Object[]{this.whiteSpaces()})).min(1);
        arrobject[1] = this.whiteSpaces();
        arrobject[2] = Character.valueOf(']');
        arrobject[3] = keys.size() == 1 ? path.addAll(keys) : path.add(new KeySet(keys));
        return this.sequence((Object)Character.valueOf('['), (Object)this.whiteSpaces(), arrobject);
    }

    Rule number() {
        return this.sequence((Object)this.optional((Object)Character.valueOf('-')), (Object)this.firstOf((Object)this.sequence((Object)this.charRange('1', '9'), (Object)this.zeroOrMore((Object)this.digit()), new Object[0]), (Object)this.digit(), new Object[0]), new Object[0]);
    }

    Rule numericKey(ListVar<? super NumericKey> keys) {
        Var from = new Var();
        return this.sequence((Object)this.number(), (Object)from.set((Object)Long.parseLong(this.match())), new Object[]{this.firstOf((Object)this.sequence((Object)"...", (Object)this.number(), new Object[]{keys.add((NumericKey)new NumberRange((Long)from.get(), Long.parseLong(this.match()), true))}), (Object)this.sequence((Object)"..", (Object)this.number(), new Object[]{keys.add((NumericKey)new NumberRange((Long)from.get(), Long.parseLong(this.match()), false))}), new Object[]{keys.add((NumericKey)new NumberKey((Long)from.get()))})});
    }

    Rule numericSet(ListVar<KeySegment> path) {
        ListVar keys = new ListVar();
        Object[] arrobject = new Object[4];
        arrobject[0] = this.join((Object)this.numericKey(keys)).using((Object)this.sequence((Object)this.whiteSpaces(), (Object)Character.valueOf(','), new Object[]{this.whiteSpaces()})).min(1);
        arrobject[1] = this.whiteSpaces();
        arrobject[2] = Character.valueOf(']');
        arrobject[3] = keys.size() == 1 ? path.addAll(keys) : path.add(new NumericSet(keys));
        return this.sequence((Object)Character.valueOf('['), (Object)this.whiteSpaces(), arrobject);
    }

    Rule indexer(ListVar<KeySegment> path) {
        return this.firstOf((Object)this.keySet(path), (Object)this.numericSet(path), new Object[0]);
    }

    Rule falcorPath() {
        ListVar<KeySegment> path = new ListVar<KeySegment>();
        return this.sequence((Object)this.whiteSpaces(), (Object)this.firstOf((Object)this.identifier(path), (Object)this.indexer(path), new Object[0]), new Object[]{this.zeroOrMore((Object)this.firstOf((Object)this.sequence((Object)Character.valueOf('.'), (Object)this.identifier(path), new Object[0]), (Object)this.indexer(path), new Object[0])), this.whiteSpaces(), EOI, this.push((Object)FalcorPath.ofAll(path))});
    }

    public FalcorPath parse(String input) {
        ListeningParseRunner runner = new ListeningParseRunner(this.falcorPath());
        ParsingResult result = runner.run((CharSequence)input);
        if (!result.isSuccess()) {
            throw new IllegalArgumentException(input);
        }
        return (FalcorPath)result.getTopStackValue();
    }
}

